#---
#layout: post
#title: 'Cassette'
#born: '2019'
#date: '2019-09-10 12:10:00 -0300'
#categories: editorial
#author: Enrique Anderson Imbert
#create: export-pdf --title="cassette" --author="Enrique Anderson Imbert" --press="pixel2pixel" --template="a6template.tex" --no-day cassette.md
#---
Enrique Anderson Imbert



>Año: 2132.  Lugar: aula de cibernética.  Personaje: un niño de nueve años.

Se llama Blas. Por el potencial de su genotipo ha sido escogido para la clase Alfa. O sea, que cuando crezca  pasará a integrar ese medio por ciento de la población mundial que se encarga del progreso. Entretanto, lo educan con rigor.

La educación, en los primeros grados, se limita al presente: que Blas comprenda el método de la ciencia y se familiarice con el uso de los aparatos de comunicación. Después, en los grados intermedios, será una educación para el futuro: que descubra, que invente. La educación en el conocimiento del pasado todavía no es materia para su clase Alfa: a lo más, le cuentan una que otra anécdota en la historia de la tecnología.

 Está en penitencia. Su tutor lo ha encerrado para que no se distraiga y termine el deber de una vez.
Blas sigue con la vista una nube que pasa. Ha aparecido por la derecha de la ventana y muy airosa se dirige hacia la izquierda.

Quizás es la misma nube que otro niño, antes que él naciera, siguió con la vista en una mañana como ésta y al seguirla pensaba en un niño de una época anterior que también la miró y en tanto la miraba creía recordar a otro niño que en otra vida... Y la nube ha desaparecido.

 Ganas de estudiar, Blas no tiene. Abre su cartera y saca, no el dispositivo calculador, sino un juguete. Es una cassette.
 Empieza a ver una aventura de cosmonautas. Cambia y se pone a escuchar un concierto de música estocástica.

Mientras ve y oye, la imaginación se le escapa hacia aquellas gentes primitivas del siglo XX a las que justamente ayer se refirió el tutor en un momento de distracción.
 ¡Cómo se habrán aburrido, sin esa cassette!
 "Allá, en los comienzos de la revolución tecnológica - había comentado el tutor - los pasatiempos se sucedían como lentos caracoles.

Un pasatiempo cada cincuenta años: de la pianola a la grabadora, de la radio a la televisión, del cine mudo y monocromo al cine parlante y policromo.
 ¡Pobres! ¡Sin esta cassette cómo se habrán aburrido!

Blas en su vertiginoso siglo XXII, tiene a su alcance miles de entretenimientos. Su vida no transcurre en una ciudad sino en el centro del universo.

La cassette admite los más remotos sonidos e imágenes; transmite noticias desde satélites que viajan por el sistema solar; emite cuerpos en relieve; permite que él converse, viéndose las caras, con un colono de Marte; remite sus preguntas a una máquina computadora cuya memoria almacena datos fonéticamente articulados y él oye las respuestas.

>  (Voces, voces, voces, nada más que voces pues en el año 2132 el lenguaje es únicamente oral: las informaciones importantes se difunden mediante fotografías, diagramas, guiños eléctricos, signos matemáticos.

En vez de terminar el deber Blas juega con la cassette. Es un paralepípedo de 20 x 12 x 3 que, no obstante su pequeñez, le ofrece un variadísimo repertorio de diversiones.

Sí, pero él se aburre. Esas diversiones ya están programadas. Un gobierno de tecnócratas resuelve qué es lo que debe ver y oír. Blas da vueltas a la cassette entre las manos. La enciende, la apaga. ¡Ah, podrán presentarle cosas para que élpiense sobre ellas pero no obligarlo a que piense así o asá!
     Ahora, por la derecha de la ventana, reaparece la nube. No es nube, es él, él mismo que anda por el aire. En todo caso, es alguien como él, exactamente como él. De pronto a Blas se le iluminan los ojos:

¿No sería posible - se dice - mejorar esta cassette, hacerla más simple, más cómoda, más personal, más íntima, más libre, sobre todo más libre?

Una cassette también portátil, pero que no dependa de ninguna energía microelectrónica: que funcione sin necesidad de oprimir botones; que se encienda apenas se la toque con la mirada y se apague en cuanto se le quite la vista de encima; que permita seleccionar cualquier tema y seguir su desarrollo hacia adelante, hacia atrás repitiendo un pasaje agradable o saltándose uno fastidioso... Todo esto sin molestar a nadie, aunque se esté rodeado de muchas personas, pues nadie, sino quien use tal cassette, podría participar en la fiesta.

Tan perfecta sería esa cassette que operaría directamente dentro de la mente. Si reprodujera, por ejemplo, la conversación entre una mujer de la Tierra y el piloto de un navío sideral que acaba de llegar de la nebulosa Andrómeda, tal cassette la proyectaría en una pantalla de nervios. La cabeza se llenaría de seres vivos. Entonces uno percibiría la entonación de cada voz, la expresión de cada rostro, la descripción de cada paisaje, la intención de cada signo... Porque claro, también habría que inventar un código de signos. No como esos de la matemática sino signos que transcriban vocablos: palabras impresas en láminas cosidas en un volumen manual. Se obtendría así una portentosa colaboración entre un artista solitario que crea formas simbólicas y otro artista solitario que las recrea...

¡Esto sí que será una despampanante novedad! - exclama el niño -. El tutor me va a preguntar: "¿Terminaste ya tu deber?" "No", le voy a contestar. Y cuando rabioso por mi desparpajo, se disponga a castigarme otra vez, ¡zas! lo dejo con la boca abierta: "¡Señor, mire en cambio qué proyectazo le traigo!"...
     (Blas nunca ha oído hablar de su tocayo Blas Pascal, a quien el padre encerró para que no se distrajera con las ciencias y estudiase las lenguas. Blas no sabe que así como en 1632 aquel otro Blas de nueve años, dibujando con tiza en la pared, reinventó la Geometría de Euclides, él, en 2132, acaba de reinventar el libro.)
