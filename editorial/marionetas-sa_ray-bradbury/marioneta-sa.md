
Caminaban lentamente por la calle, a eso de las diez de la noche, hablando con
tranquilidad. No tenían más de treinta y cinco años. Estaban muy serios.

-Pero ¿por qué tan temprano? -dijo Smith.

-Porque sí -dijo Braling.

-Tu primera salida en todos estos años y te vuelves a casa a las diez.

-Nervios, supongo.

-Me pregunto cómo te las habrás ingeniado. Durante diez años he tratado de sacarte a
beber una copa. Y hoy, la primera noche, quieres volver en seguida.

-No tengo que abusar de mi suerte -dijo Braling.

-Pero, ¿qué has hecho? ¿Le has dado un somnífero a tu mujer?

-No. Eso sería inmoral. Ya verás.
Doblaron la esquina.

-De veras, Braling, odio tener que decírtelo, pero has tenido mucha paciencia con ella.
Tu matrimonio ha sido terrible.

-Yo no diría eso.

-Nadie ignora cómo consiguió casarse contigo.

Allá, en 1979, cuando ibas a salir para Río.

-Querido Río. Tantos proyectos y nunca llegué a ir.

-Y cómo ella se desgarró la ropa, y se desordenó el cabello, y te amenazó con llamar a la
policía si no te casabas con ella.

-Siempre fue un poco nerviosa, Smith, entiéndelo.

-Había algo más. Tú no la querías. Se lo dijiste, ¿no es así?

-En eso siempre fui muy firme.

-Pero sin embargo te casaste.

-Tenía que pensar en mi empleo, y también en mi madre, y en mi padre. Una cosa así
hubiese terminado con ellos.

-Y han pasado diez años.

-Sí -dijo Braling, mirándolo serenamente con sus ojos grises-. Pero creo que todo va a
cambiar. Mira.

Braling sacó un largo billete azul.

-¡Cómo! ¡Un billete para Río! ¡El cohete del jueves!

-Sí, al fin voy a hacer mi viaje.

-¡Es maravilloso! Te lo mereces de veras. Pero, ¿y tu mujer, no se opondrá? ¿No te hará una escena?
Braling sonrió nerviosamente.-No sabe que me voy. Volveré de Río de Janeiro dentro de un mes y nadie habrá
notado mi ausencia, excepto tú.
Smith suspiró.

-Me gustaría ir contigo.

-Pobre Smith, tu matrimonio no ha sido precisamente un lecho de rosas, ¿eh?

-No, exactamente. Casado con una mujer que todo lo exagera. Es decir, después de
diez años de matrimonio, ya no esperas que tu mujer se te siente en las rodillas dos horas todas las noches; ni que te llame al trabajo doce veces al día, ni que te hable en media lengua. Y parece como si en este último mes se hubiese puesto todavía peor. Me pregunto si no será una simple.

-Ah, Smith, siempre el mismo conservador. Bueno, llegamos a mi casa. ¿Quieres
conocer mi secreto? ¿Cómo pude salir esta noche?

-Me gustaría saberlo.

-Mira allá arriba -dijo Braling.

Los dos hombres se quedaron mirando el aire oscuro.
En una ventana del segundo piso apareció una sombra. Un hombre de treinta y cinco
años, de sienes canosas, ojos tristes y grises y bigote minúsculo se asomó y miró hacia
abajo.

-Pero, cómo, ¡eres tú! -gritó Smith.

-¡Chist! ¡No tan alto!
Braling agitó una mano.
El hombre respondió con un ademán y desapareció.

-Me he vuelto loco -dijo Smith.

-Espera un momento.
Los hombres esperaron.

Se abrió la puerta de calle y el alto caballero de los finos bigotes y los ojos tristes salió
cortésmente a recibirlos.

-Hola, Braling -dijo.

-Hola, Braling Dos-dijo Braling.
Eran idénticos.
Smith abría los ojos.

-¿Es tu hermano gemelo? No sabía que...

-No, no -dijo Braling serenamente-. Inclínate. Pon el oído en el pecho de Braling Dos.
Smith titubeó un instante y al fin se inclinó y apoyó la cabeza en las impasibles
costillas.
Tic-tic-tic-tic-tic-tic-tic-tic.-¡Oh, no! ¡No puede ser!

-Es.

-Déjame escuchar de nuevo.

Tic-tic-tic-tic-tic-tic-tic-tic.
Smith dio un paso atrás y parpadeó, asombrado. Extendió una mano y tocó los brazos
tibios y las mejillas del muñeco.

-¿Dónde lo conseguiste?

-¿No está bien hecho?

-Es increíble. ¿Dónde?

-Dale al señor tu tarjeta, Braling Dos.

Braling Dos movió los dedos como un prestidigitador y sacó una tarjeta blanca.

>"MARIONETAS, SOCIEDAD ANÓNIMA
Nuevos Modelos de Humanoides Elásticos.
De funcionamiento garantizado.
Desde 7.600 a 15.000 dólares.
Todo de litio."

-No -dijo Smith.

-Sí -dijo Braling.

-Claro que sí -dijo Braling Dos.

-¿Desde cuándo lo tienes?

-Desde hace un mes. Lo guardo en el sótano, en el cajón de las herramientas. Mi mujer
nunca baja, y sólo yo tengo la llave del cajón. Esta noche dije que salía a comprar unos
cigarros. Bajé al sótano, saqué a Braling Dos de su encierro, y lo mandé arriba, para que
acompañara a mi mujer, mientras yo iba a verte, Smith.

-¡Maravilloso! ¡Hasta huele como tú! ¡Perfume de Bond Street y tabaco Melachrinos!

-Quizás me preocupe por minucias, pero creo que me comporto correctamente. Al fin y al
cabo mi mujer me necesita a mí. Y esta marioneta es igual a mí, hasta el último detalle.
He estado en casa toda la noche. Estaré en casa con ella todo el mes próximo. Mientras
tanto otro caballero paseará al fin por Río. Diez años esperando ese viaje. Y cuando yo
vuelva de Río, Braling Dos volverá a su cajón.
Smith reflexionó un minuto o dos.

-¿Y seguirá marchando solo durante todo ese mes? -preguntó al fin.

-Y durante seis meses, si fuese necesario. Puede hacer cualquier cosa -comer, dormir,
transpirar cualquier cosa, y de un modo totalmente natural. Cuidarás muy bien a mi mujer,
¿no es cierto, Braling Dos?-Su mujer es encantadora -dijo Braling Dos-. Estoy tomándole cariño.
Smith se estremeció.

-¿Y desde cuándo funciona Marionetas, S. A.?

-Secretamente, desde hace dos años.

-Podría yo... quiero decir, sería posible... -Smith tomó a su amigo por el codo-. ¿Me
dirías dónde puedo conseguir un robot, una marioneta, para mí? Me darás la dirección,
¿no es cierto?

-Aquí la tienes.
Smith tomó la tarjeta y la hizo girar entre los dedos.

-Gracias -dijo-. No sabes lo que esto significa. Un pequeño respiro. Una noche, una vez al
mes... Mi mujer me quiere tanto que no me deja salir ni una hora. Yo también la quiero
mucho, pero recuerda el viejo poema: «El amor volará si lo dejas; el amor volará si lo
atas.» Sólo deseo que ella afloje un poco su abrazo.

-Tienes suerte, después de todo. Tu mujer te quiere. La mía me odia. No es tan
sencillo.

-Oh, Nettie me quiere locamente. Mi tarea consistirá en que me quiera cómodamente.

-Buena suerte, Smith. No dejes de venir mientras estoy en Río. Mi mujer se extrañará si
desaparecieras de pronto. Tienes que tratar a Braling Dos, aquí presente, lo mismo que a
mí.

-Tienes razón. Adiós. Y gracias.

Smith se fue, sonriendo, calle abajo. Braling y Braling Dos se encaminaron hacia la
casa.
Ya en el ómnibus, Smith examinó la tarjeta silbando suavemente.

"Se ruega al señor cliente que no hable de su compra. Aunque ha sido presentado al
Congreso un proyecto para legalizar Marionetas, S. A., la ley pena aún el uso de los
robots."

-Bueno -dijo Smith.

"Se le sacará al cliente un molde del cuerpo y una muestra del color de los ojos, labios,
cabellos, piel, etc. El cliente deberá esperar dos meses a que su modelo esté terminado."

No es tanto, pensó Smith. De aquí a dos meses mis costillas podrán descansar al fin de
los apretujones diarios. De aquí a dos meses mi mano se curará de esta presión
incesante. De aquí a dos meses mi aplastado labio inferior recobrará su tamaño normal.
No quiero parecer ingrato, pero... Smith dio vuelta la tarjeta.

>"Marionetas, S. A. funciona desde hace dos años. Se enorgullece de poseer una larga
lista de satisfechos clientes. Nuestro lema es «Nada de ataduras.» Dirección: 43 South
Wesley."

El ómnibus se detuvo. Smith descendió, y caminó hasta su casa diciéndose a sí mismo:
Nettie y yo tenemos quince mil dólares en el banco. Podría sacar unos ocho mil con la
excusa de un negocio. La marioneta me devolverá el dinero, y con intereses. Nettie nunca
lo sabrá.

Abrió la puerta de su casa y poco después entraba en el dormitorio. Allí estaba Nettie,
pálida, gorda, y serenamente dormida.

-Querida Nettie.

>-Al ver en la semioscuridad ese rostro inocente, Smith se sintió
aplastado, casi, por los remordimientos-.

Si estuvieses despierta me asfixiarías con tus
besos y me hablarías al oído. Me haces sentir, realmente, como un criminal. Has sido una
esposa tan cariñosa y tan buena. A veces me cuesta creer que te hayas casado conmigo,
y no con Bud Chapman, aquel que tanto te gustaba. Y en este último mes has estado
todavía más enamorada que antes.

Los ojos se le llenaron de lágrimas. Sintió de pronto deseos de besarla, de confesarle su
amor, de hacer pedazos la tarjeta, de olvidarse de todo el asunto. Pero al adelantarse
hacia Nettie sintió que la mano le dolía y que las costillas se le quejaban. Se detuvo, con
ojos desolados, y volvió la cabeza. Salió de la alcoba y atravesó las habitaciones oscuras.
Entró canturreando en la biblioteca, abrió uno de los cajones del escritorio, y sacó la
libreta de cheques.

-Sólo ocho mil dólares -dijo-. No más. -Se detuvo-. Un momento.
Hojeó febrilmente la libreta.

-¡Pero cómo! -gritó-. ¡Faltan diez mil dólares! -Se incorporó de un salto-. ¡Sólo quedan cinco mil!

¿Qué ha hecho Nettie? ¿Qué ha hecho con ese dinero? ¿Más sombreros, más
vestidos, más perfumes? ¡Ya sé! ¡Ha comprado aquella casita a orillas del Hudson de la
que ha estado hablando durante tantos meses!
Se precipitó hacia el dormitorio, virtuosamente indignado. ¿Qué era eso de disponer así
del dinero? Se inclinó sobre su mujer.

-¡Nettie! -gritó-. ¡Nettie, despierta!
Nettie no se movió.

-¡Qué has hecho con mi dinero! -rugió Smith.
Nettie se agitó, ligeramente. La luz de la calle brillaba en sus hermosas mejillas.
A Nettie le pasaba algo. El corazón de Smith latía con violencia. Se le secó la boca. Se
estremeció. Se le aflojaron las rodillas.

-¡Nettie, Nettie! -dijo-. ¿Qué has hecho con mi dinero?
Y en seguida, esa idea horrible. Y luego el terror y la soledad. Y luego el infierno, y la
desilusión. Smith se inclinó hacia ella, más y más, hasta que su oreja febril descansó,
firmemente, irrevocablemente, sobre el pecho redondo y rosado.-¡Nettie! -gritó.
Tic-tic-tic-tic-tic-tic-tic-tic-tic-tic-tic-tic-tic-tic...

Mientras Smith se alejaba por la avenida, internándose en la noche, Braling y Braling
Dos se volvieron hacia la puerta de la casa.

-Me alegra que él también pueda ser feliz -dijo Braling.

-Sí -dijo Braling Dos distraídamente.

-Bueno, ha llegado la hora del cajón, Braling Dos.

-Precisamente quería hablarle de eso -dijo el otro Braling mientras entraban en la casa-.
El sótano. No me gusta. No me gusta ese cajón.

-Trataré de hacerlo un poco más cómodo.

-Las marionetas están hechas para andar, no para quedarse quietas. ¿Le gustaría
pasarse las horas metido en un cajón?

-Bueno...

-No le gustaría nada. Sigo funcionando. No hay modo de pararme. Estoy perfectamente
vivo y tengo sentimientos.
-Esta vez sólo será por unos días. Saldré para Río y entonces podrás salir del cajón.
Podrás vivir arriba.
Braling Dos se mostró irritado.

-Y cuando usted regrese de sus vacaciones, volveré al cajón.

-No me dijeron que iba a vérmelas con un modelo difícil.

-Nos conocen poco -dijo Braling Dos-. Somos muy nuevos. Y sensitivos. No me gusta
nada imaginarlo al sol, riéndose, mientras yo me quedo aquí pasando frío.

-Pero he deseado ese viaje toda mi vida -dijo Braling serenamente.
Cerró los ojos y vio el mar y las montañas y las arenas amarillas.

El ruido de las olas le acunaba la mente. El sol le acariciaba los hombros desnudos. El vino era magnífico.

-Yo nunca podré ir a Río -dijo el otro-. ¿Ha pensado en eso?

-No, yo...

-Y algo más. Su esposa.

-¿Qué pasa con ella? -preguntó Braling alejándose hacia la puerta del sótano.

-La aprecio mucho.
Braling se pasó nerviosamente la lengua por los labios.

-Me alegra que te guste.

-Parece que usted no me entiende. Creo que... estoy enamorado de ella.Braling dio un paso adelante y se detuvo.

-¿Estás qué?

-Y he estado pensando -dijo Braling Dos- qué hermoso sería ir a Río, y yo que nunca podré ir...
Y he pensado en su esposa y... creo que podríamos ser muy felices, los dos, yo y ella.
-M-m-muy bien.-Braling caminó haciéndose el distraído hacia la puerta del sótano-.
Espera un momento, ¿quieres? tengo que llamar por teléfono.
Braling Dos frunció el ceño.

-¿A quién?

-Nada importante.

-¿A Marionetas, Sociedad Anónima? ¿Para decirles que vengan a buscarme?

-No, no... ¡Nada de eso!
Braling corrió hacia la puerta. Unas manos dc hierro lo tomaron por los brazos.

-¡No se escape!

-¡Suéltame!

-No.

-¿Te aconsejó mi mujer hacer esto?

-No.

-¿Sospechó algo? ¿Habló contigo? ¿Está enterada?
Braling se puso a gritar. Una mano le tapó la boca.

-No lo sabrá nunca, ¿me entiende? No lo sabrá nunca.
Braling se debatió.

-Ella tiene que haber sospechado. ¡Tiene que haber influido en tí!

-Voy a encerrarlo en el cajón. Luego perderé la llave y compraré otro billete para Río,
para su esposa.

-¡Un momento, un momento! ¡Espera! No te apresures. Hablemos con tranquilidad.

-Adiós, Braling.
Braling se endureció.

-¿Qué quieres decir con «adiós»?
Diez minutos más tarde, la señora Braling abrió los ojos. Se llevó la mano a la mejilla.
Alguien la había besado. Se estremeció y alzó la vista.

-Cómo... No lo hacías desde hace años -murmuró.

-Ya arreglaremos eso -dijo alguien.
