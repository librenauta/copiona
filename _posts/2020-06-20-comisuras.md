---
layout: post
title: '#Comisuras'
born: '2020'
date: '2020-06-20 20:35:00 -0300'
categories: licuadora
author: librenauta & cian
---

# Comisuras, recetas para disfrutar~

>Comisuras fue nombrado por un **autocorrect**, el día que quise escribir "comiditas" en un mensaje de txt.

[Comisuras](https://comisuras.copiona.com) es un backup de recetas que voy haciendo con @soycian de vez en cuando para docuentar qué comemos. También para compartir las recetas y los remix de las recetas que alguna vez nos compartieron, amigues, madres, abuelas e internet.

![img-1]({{ site.url }}/img/licuadora/comisuras.png)

Las recetas son uno de los mejores ejemplos de cómo la cultura se transforma, de cómo cada une le pone su "toque", gusto , experiencia en la creación de una comida. las recetas son libres, y como libres las compartimos con ustedes, como alguien nos las compartió a nosotzs.

> visitar [--> Comisuras](https://comisuras.copiona.com)
