---
layout: post
title: '#Copyfight!'
born: '2021'
date: '2021-02-06 13:18:00 -0300'
categories: licuadora
author: librenauta
---

### _PUM!_

![copyfight]({{ url.site }}/img/log/copyfight-2.jpg)

Siempre que se habla en internet de la idea de "copiar un libro es robar" lo primero que me pregunto es ¿Cómo se roba algo que otra nunca deja de poseer? por ejemplo, un mp3 o un pdf con el último libro escaneado de Mark Fisher. Después viene a mi mente la "Kopimashin", ese maravilloso "DTS" (dispositivo de trolleo de silicio) que construyó Peter Sunde responsable de contener el tema "Gnarls Barkley’s - Crazy.mp3” y que lo copia y borra 100 veces por segundo en loop y genera "pérdidas" teóricas por más de 150 millones de dólares a las industrias culturales.

Este loop propio del artivismo de la cultura libre es el ejemplo perfecto de lo irracional que son las "pérdidas monetarias por cada copia pirata", así de fácil es volverse millonario post 00', así de fácil es matar el argumento de las corporaciones con un simple script en python*.
copiar _nunca_ es robar sí es multiplicar *inserte capitalismo de escasez aquí*; copiar es la forma de crear y aprender; copiar permite entender procesos, técnicas; copiar es parte de la formación y creación de cada idea proyectada en bienes culturales.

Estamos envueltas y caladas muy profundamente por las nociones occidentales de la originalidad, ese manto que atrapa, cristaliza e intenta inmortalizar una creación artística como única e -irrepetible- como si pudiéramos realmente conservar esa esencia evitando el paso del tiempo, como si todavía pensáramos en los genios y la musa, como si dejáramos de lado la producción colectiva y esa famosa frase de "construimos sobre los hombros de otros gigantes".
 Si podemos pensar un mundo fuera de esta occidental y colonizante burbuja de la cultura podemos observar a oriente y su interpretación de la creación como un flujo de transformación constante: "el lejano oriente es ajeno a este culto del original" escribe Byung-chul-han en Shanzhai.
Oriente interpreta a las copias como originales porque su técnica de mantenimiento es una reconstrucción constante, en términos occidentales, se estaría profanando las obras constantemente. esta diferencia conceptual genera que el original sea un imaginario para ellos.

Sin copias no existe hip hop, ni collage, ni remix, ni memes! y como planeamos no quedarnos sin principalmente (MEMES), es que creamos guías de batalla contra la normalización del "acceso" a la cultura. Tener acceso a un archivo no es lo mismo que poseerlo; es triste cómo la evolución del capitalismo de plataformas lleva a mantenernos estancadas y dependientes de las infraestructuras centralizadas de información con las lógicas que cercan el contenido dentro de muros de pago -que obvio nos dedicamos a saltar-.

La aparición de internet, con sus dispositivos que copiaban información a costo cero, nos trajo una oportunidad unica. Cada copia que haciamos de un .mp3 nos traía el original, dicho de otro modo, todas las copias son originales _bit by bit_. Así es como vivíamos o intentábamos vivir en este universo de abundancia de datos y cultura los primeros 15 años de la vida de  internet hasta que comenzaron a operar las industrias culturales sobre los cables de nuestras redes comunitarias de información (p2p, torrent) y lograron encerrar muchos de los contenidos en plataformas de "stream" que ya conocemos.

Acceder de forma online no es apropiarse. Descargar, guardar, compartir en un mail, bajar a un pendrive, hacer un torrent es apropiarse la cultura; hacerla disponible para otras en plataformas colectivas es hacer la cultura.
Pero las prácticas de colectivización y expropiación las piratas que habitamos la interdimensión de la red las tenemos muy estudiadas, queda descifrar cómo vincular a las artistas productoras de contenido con las formas de compartir y capitalizar su trabajo sin que sean cooptadas por las plataformas de la maquinaria antes del primer parpadeo de remix.

**-No hay futuro sin eliminar la propiedad intelectual-**
