---
layout: post
title: '#Collage para Telegrafica RedMundialCasera'
born: '2018'
date: '2018-01-10 10:10:00 -0300'
categories: diseño
author: librenauta
---
# /Obediencia Tecnológica

_Tele_gráfica fue un proyecto autogestivo de taller [Caput by gustako](https://gustako.wordpress.com/)
me convocaron a participar con un collage :3
![obediencia]({{ url.site }}/img/dis/telegrafica.png)
1. [obediencia tecnológica para descargar en .svg]({{ url.site }}/img/dis/telegrafica.svg)

# 2020 - COVID-19
> este collage está en la portada del dossier [desobediencia.partidopirata.com.ar](https://desobediencia.partidopirata.com.ar)
