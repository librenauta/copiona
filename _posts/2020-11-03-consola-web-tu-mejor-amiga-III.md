---
layout: post
title: '/Consola web tu mejor amiga III [vimeo]'
born: '2020'
date: '2020-11-03 23:59:00 -0300'
categories: licuadora
author: librenauta
background: /img/url.png
tags:
  - cultura libre
  - videos libres
  - descargar video
---
### _Consola web for fun and piracy_

# Bajandole
Si ya pagaste un curso y te dan acceso a un campus para verlos, ¿Por qué luego no podrías ver ese contenido?, para poder repasar esos videos cuando quieras vamos a descargarlos. porque si tu navegador puede reproducirlos de alguna manera:
1. Se estan bajando a tu pc.
2. Podemos conseguir el link directo donde está hosteado el video.
*Para esto necesitamos navegar los siguientes pasos y llegar a la: [URL video]+[TOKEN VALIDACIÓN]

[1] Dentro del campus, hacemos Click-derecho [_Inspeccionar elementos_]
![683x741]({{ site.url }}/img/licuadora/consola-3/consola-web-3-1.png "Large example image")

[2] Vamos a la pestaña [_Depurador_]
![683x741]({{ site.url }}/img/licuadora/consola-3/consola-web-3-2.png "Large example image")

[3] Vamos a la pestaña [_player.vimeo.com_]
![683x741]({{ site.url }}/img/licuadora/consola-3/consola-web-3-3.png "Large example image")

[4] Buscamos la carpeta [_video_] index
![683x741]({{ site.url }}/img/licuadora/consola-3/consola-web-3-4.png "Large example image")

[5] Seleccionamos el contenido de index
![683x741]({{ site.url }}/img/licuadora/consola-3/consola-web-3-5.png "Large example image")

[6] Lo pegamos en un editor de texto donde podamos buscar con ctrl+f ".mp4" y copiamos una url que contenga el link directo + token de validación
![683x741]({{ site.url }}/img/licuadora/consola-3/consola-web-3-6.png "Large example image")

[6] y con esta url completa podemos llegar al video completo y su descarga.  *Ej:``https://gcs-vimeo.akamaized.net/exp=15++++++~acl=%2A%2F1047741295.mp4%2A~hmac=2abc71f0c50b40f5d47f722d79de74f7649508e2988934723e0568a86b4780e9/vimeo-prod-skyfire-std-us/01/4477/++++++/1047741295.mp4``
 dónde: ``https://gcs-vimeo.akamaized.net/exp=15++++++~acl=%2A%2F1047741295.mp4`` es la url y si la ingresamos nos dice "acceso denegado" pero si agregamos:`` %2A~hmac=2abc71f0c50b40f5d47f722d79de74f7649508e2988934723e0568a86b4780e9/vimeo-prod-skyfire-std-us/01/4477/10/++++++/1047741295.mp4`` podemos descargarlo.
![683x741]({{ site.url }}/img/licuadora/consola-3/consola-web-3-7.png "Large example image")

*Luego en una terminal ponemos ``$ wget [link]`` y descargamos!
@Piratacland armó este tutorial para poder descargar los subtitulos de vimeo y me pareció super interesante
[THIS](https://blognooficial.wordpress.com/2017/10/17/descargar-los-subtitulos-de-vimeo-actualizacion/)

GO PIRATE!

~librenauta
