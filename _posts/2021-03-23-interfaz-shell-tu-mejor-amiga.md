---
layout: post
title: '/Interfaz shell tu mejor amiga [Barca Streaming]'
born: '2021'
date: '2021-03-23 13:59:00 -0300'
categories: licuadora
author: librenauta & piratas
background:
tags:
  - cultura libre
  - video stream
  - cytu.be
  - barca Streaming
---
### _shell skills for fun and piracy_

# Streaming de videos subtitulados via cytu.be

cytu.be permite ingresar una url y reproducir una video con sus subtitulos en streaming a varias personas de forma sync:

pero esta forma de transmitir un video tiene algunos requerimientos:

1. El video tiene que estar en .mp4
2. Los subtítulos tienen que estar en .vtt y codigicación UTF-8 (para que salgan bien los tíldes)
3. Crear un json con la información (para que cytube busque el video junto con el subtítulo)

_Vamos paso a paso:_

## 1 - Video

Para esta parte es necesario tener [ffmpeg](https://ffmpeg.org/download.html) instalado en tu máquina
si tenés la pelicula/documental en .mkv u otro formato con estas -flags lo convertis a .mp4:

a) Si el video contiene subtitulos, convertis el .mkv a .mp4 y los subtitulos de -map 0s los extraes en peli.vtt
`ffmpeg -i peli.mkv -c:v copy -c:a libopus -ac 2 -b:a 128k peli.mp4 -map 0:s peli.vtt`

b) Si tu video no contiene subtitulos incrustados podés hacer
`ffmpeg -i peli.mkv -c:v copy -c:a copy peli.mp4`

## 2 - .srt a .vtt

for sub in *.srt; do ffmpeg -i $sub $sub.vtt; done
(con esto pasamos todos los  subtitulos que tengamos en el directorio que estamos paradzs a .vtt)

si nos aparece un error como este:
```
[srt @ 0x55da4235bf80] Invalid UTF-8 in decoded subtitles text; maybe missing -sub_charenc option
Error while decoding stream #0:0: Invalid data found when processing input
```
quiere decir que nuestro subtitulo.srt esta en una codificacion diferente a UTF-8, para cambiar la codificación, podemos abrir el subtitulo con Gedit (editor de texto) y guardarlo como UTF-8 en el box de codificación.


## 3 - crear el json con pyton by @unperson

Para esto vamos a usar el script que hizo @unperson en pyton, primero editaremos la linea 7 que dice (URL="")con la url donde estará hosteado el video y los subtitulos. Luego podemos guardar este código con un nombre por ejemplo convertjson.py

<code>
#!/usr/bin/env python3
import sys
from subprocess import check_output
import json
from os import path, listdir
from urllib.parse import quote
URL="https://example.com"
file = sys.argv[1]
dirname, filename = path.split(file)
root, extension = path.splitext(filename)
ffprobe = check_output([*"ffprobe -v quiet -show_format -of json".split(' '), file])
fformat = json.loads(ffprobe)['format']
out = {
    'title': root,
    "duration": float(fformat['duration']),
    "live": False,
    "sources": [
        {
          "url": URL + quote(file),
          "contentType": "video/" + extension[1:],
          "quality": 1080
        }
    ],
}
textTracks = []
for file in listdir('./' + dirname):
    if file.startswith(root + '.') and file.endswith(".vtt"):
        lang = file[len(root + '.'):-len(".vtt")]
        textTracks.append({
            'url': URL + quote(path.join(dirname, file)),
            'contentType': "text/vtt",
            'name': "Subtitles " + lang
        })
if textTracks: out['textTracks'] = textTracks
json.dump(out, sys.stdout, indent=2)
</code>

 Lo ponemos en el mismo directorio que tengamos la película/documental en .mp4 y los subtitulos .vtt

luego: `python convertjson.py peli.mp4`

y nos genera algo como esto:
```
{
  "title": "peli",
  "duration": 4773.333,
  "live": false,
  "sources": [
    {
      "url": "https://url-host/peli.mp4",
      "contentType": "video/mp4",
      "quality": 1080
    }
  ],
  "textTracks": [
    {
      "url": "https://url-host/peli.vtt",
      "contentType": "text/vtt",
      "name": "Subtitles "
    }
  ]
}
```

Luego vamos a [cytu.be](https://cytu.be) y con una cuenta creada podemos agregar el json al pulsar [+]
![cytu.be]({{ site.url }}/img/licuadora/cytu.be.png)

_agradecimientos a la barca streaming, @unperson && @void que me ayudaron con esto <3_

GO PIRATE!

~librenauta
