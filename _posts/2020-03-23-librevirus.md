---
layout: post
title: '#Librevirus.cc'
born: '2020'
date: '2020-03-23 22:52:00 -0300'
categories: proyectos
author: dani - librenauta
---
## bibliotecas confinadas

En tiempos de encerramiento global, abrirse a compartir se convierte en uno de los actos más naturales. Mientras que las personas nos quedamos en casa, los bits siguen fluyendo de forma torrencial a través de los cables y las redes. ¿Dónde viven los datos? ¿Dónde estoy cuando entro en una web? ¿Quién puso esa información ahí Preguntas que se agolpan ahora que estamos conectados a la red con una constancia aún mayor que antes.

Si la nube es la computadora de otra persona,Librevirus es el contagio de la hospitalidad dejando entrar en nuestras casas digitales a todas las personas que lo deseen. Porque cuando entramos a una web, en realidad entramos a una casa; y todas las bibliotecas que veas aquí están hospedadas dentro de las casas de quienes las crearon, ni en servidores externos ni con empresas que vigilarán tus pasos.

Librevirus es un repositorio orgánico de librerías libres creadas por cualquier persona que tiene ganas de aprender a hacer cosas por sí misma y el deseo de compartir lo que tiene para hacerse más rica.

Ya tengas una RaspberryPi, un ordenador/computadora, conexión a Internet, curiosidad por saber cómo hacer un repositorio propio, ganas de tocar aquí y allá, romper cosas, arreglarlas, volver a romperlas y arreglarlas con otras personas, libros que te apetece compartir y un poco de tiempo… entonces crea tu propio Librevirus, o simplemente, disfruta de los que encuentras

--> [librevirus.cc](https://librevirus.cc)

--> [Documentación](https://librevirus.cc/docs.html)

--> [Mi biblioteca virtual](https://bibliobox.librevirus.cc:8080), estará fuera de linea del 12/08/2021 al 25/08/2021, porque me llevo  la raspi de paseo en modo [bibliobox](https://bibliobox.copiona.com) al sur



![librevirus]({{ url.site }}/img/librevirus.jpg)
