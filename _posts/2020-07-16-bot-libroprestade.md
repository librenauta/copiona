---
layout: post
title: '#Bot @unlibroprestado'
born: '2020'
date: '2020-07-16 22:59:00 -0300'
categories: licuadora
author: librenauta
---

# /Botcito de libritos

### Ilustraciones científicas de libros que nos presta archive.org

visitando este libro: [Pilze der Heimat ; eine Auswahl der verbreitesten, essbaren ungeniessbaren und giftigen Pilze unserer Wlder (Fungi)](https://archive.org/details/pilzederheimatei00gram/page/n100/mode/1up) me topé con que cada página del libro en archive lo podés ver de forma individual al hacer _click derecho_ + _ver imagen_.

> Cada imagen es una consulta en php al servidor, y no está hardcodeada <3, modificando los parámetros podríamos obtener todas las urls.

Esto te lleva al link de esa imagen dentro del archive <3, ej.:


`https://ia800200.us.archive.org/BookReader/BookReaderImages.php?zip=/13/items/pilzederheimatei00gram/pilzederheimatei00gram_jp2.zip&file=pilzederheimatei00gram_jp2/pilzederheimatei00gram[_0063.jp2&scale=9.408284023668639&rotate=0]()`


Esta última parte de la url es importante y la podemos interpretar de esta forma:

1. [*_0063.jp2*]() es el número de página, al parecer archive las sube consecutivas, asique es posible solo cambiando ese número visitar todas las páginas del libro.

2. [*&scale=9.408284023668639*]() es la escala, yo cuando generé las urls lo cambie a 1, el valor 1 parece ser el de máxima resolución.

3. [*&rotate=0*]() es la orientación, algunas páginas están rotadas, es posible asignar la orientación en la misma url poniéndole =90 por ejemplo.

![img-1]({{ site.url }}/img/licuadora/bot/bot-1.jpg)

de esta forma tenemos las urls de todas las páginas del libro con la resolución/orientación.

Ahora el bot'cito esta creado con [cheapbotsdonequick](https://cheapbotsdonequick.com/) que es una web muy simple donde se permite linkear una cuenta de twitter y asignar bloques de [json](https://es.wikipedia.org/wiki/JSON) con la información a twittear de forma periódica.


Este bloque el *json* requiere que ingresemos una variable y luego asignamos los posibles resultados de esa o esas variables (pueden ser varias).

este es el ejemplo para postear 2 imágenes:

```JSON
{
    "origin": ["\\#fungi \\#nature \\#book \\#archive \\#scientificillustration #alternatives#"],
    "alternatives" : [

    "{img https://ia903102.us.archive.org/BookReader/BookReaderImages.php?zip=/29/items/CAT11016849
        CAT11016849_jp2.zip&file=CAT11016849_jp2/CAT11016849_0233.jp2&scale=1&rotate=0}",

    "{img https://ia903102.us.archive.org/BookReader/BookReaderImages.php?zip=/29/items/CAT11016849/
        CAT11016849_jp2.zip&file=CAT11016849_jp2/CAT11016849_0237.jp2&scale=1&rotate=0}"

    ]
}
```

donde

 ´"origin": [] es el total de lo que voy a postear´

luego dentro de origin tenemos:

 `"\\#fungi \\#nature \\#book \\#archive \\#scientificillustration #alternatives#"]`

la doble barra invertida `\\` es utilizada para escapar el # y que se lea solo como carácter de texto y no un carácter especial, esto permite poner Hashtags en los tweets.

dentro de las "", tenemos *#alternatives* que sería la variable que se reemplaza por los links de las imágenes posteriores.

links:
```JSON
"{img https://ia903102.us.archive.org/BookReader/BookReaderImages.php?zip=/29/items/CAT11016849/
    CAT11016849_jp2.zip&file=CAT11016849_jp2/CAT11016849_0233.jp2&scale=1&rotate=0}",
```
nota: es importante sacar la última coma de la lista de las urls dentro de #alternatives para respetar  la sintaxis del json

![img-1]({{ site.url }}/img/licuadora/bot/bot-2.png)

pueden ver los resultados aquí: [@unlibroprestado](https://twitter.com/unlibroprestado)

##  ~update  20-07-2020

Para poder ingresar descripción de cada libro asigné una variable por cada uno nuevo agregado.

```JSON
{
  "origin": ["#alternatives# \\#fungi \\#nature \\#book \\#archive \\#scientificillustration "],

"fuente1": ["titulo libro  https://paginadereferencia"],
"fuente2": ["titulo libro  https://paginadereferencia"],

 "alternatives" : [

"#fuente1#{img url-imagen}",

"#fuente2#{img url-imagen}"

]
}
```

## ~update 2 04-08-2020

Archive.org modificó las urls y les agregó un id=hash entre jp2&Scale para tener en cuenta. Cada libro tiene un ID diferente.
