---
layout: post
title: '#TAPA "Queremos redes libres"'
born: '2018'
date: '2018-01-10 10:10:00 -0300'
categories: diseño
author: librenauta
---
# Queremos redes libres, edición mx del zine hecho por el [PIP](https://partidopirata.com.ar)

y nuevamente editado para la campaña [\<Salvemos internet>](https://salvemosinternet.tk/) con aportes de diferentes territorios de América latina y el caribe.


![queremos-redes-libres]({{ url.site }}/dis/queremos-redes-libres-ed-mx/cover.png)
diseño de tapa by @librenauta
1. [descargar el txt en epub]({{ url.site }}/dis/queremos-redes-libres-ed-mx/redes-libres.epub)
2. [descargar el txt en pdf]({{ url.site }}/dis/queremos-redes-libres-ed-mx/redes-libres.pdf)
3. [descargar la portada en svg]({{ url.site }}/dis/queremos-redes-libres-ed-mx/cover.svg)
4. [descargar portada y contratapa en pdf]({{ url.site }}/dis/queremos-redes-libres-ed-mx/redes-libres_forros.pdf)

## Por otras telecomunicaciones posibles

> Conoce sobre la lucha para salvar internet y la búsqueda de redes autónomas y comunitarias en diversas regiones de América Latina y el Caribe.
