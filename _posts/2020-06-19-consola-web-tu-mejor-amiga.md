---
layout: post
title: '#Consola web tu mejor amiga'
born: '2020'
date: '2020-06-19 23:59:00 -0300'
categories: licuadora
author: librenauta
---

# /Consola web tu mejor amiga

### _Consola web for fun and piracy_

1. entro a un lugar donde venden tipografías [je](http://www.ultratypes.com/) , [je2](https://konstantinestudio.com) && [vj-type](https://vj-type.com/)

2. puedo testear la fuente en un bloque de html y esto me resulta muy gracioso ya que si estoy viendo la typo, la estoy descargando en algún /tmp y es posible llegar al _link_

3. inspecciono elementos y veo la fuente en .woff en la pestaña "tipografías"

![img-1]({{ site.url }}/img/licuadora/consola-web1.png)

![img-1]({{ site.url }}/img/licuadora/consola-web2.png)

4. la bajo accediendo al link :3

5. el archivo que bajé ".woff" se convierte a lo que quieras, por ejemplo a [otf con un poco de python 3](https://github.com/hanikesn/woff2otf) para poder usarla con [inkscape](https://inkscape.org/es/)

si bajaste muchas podes usar un for en bash para convertirlas todas de una:

este es mi directorio:
type/
type/woff2otf.py
type/example.woff
cd 
en la terminal parados en la carpeta donde esta el woff2otf.py y las tipografias en .woff:

`for i in *.woff ; do python ./woff2otf.py "$i" "${i%.*}.otf" ; done`

con este for recorremos todos los archivos que son .woff y los convertimos a .otf

6. también estan estos servicios "online" para [convertir](https://convertio.co/es/woff-otf/) ".woff" o ".woff2" a *.otf* para la que no tenga una terminal a mano :D

$ python ./woff2otf.py oxymora-outline-webfont.woff oxymora-outline-webfont.otf

- alf ／占~~~~~ [suma este link](https://www.httrack.com/) para descargar toda una web entera :O

## ~Update 07/08/2020

Me topé con la web [fontfabric](https://www.fontfabric.com) | [optimo.ch](https://optimo.ch) que por cada fuente hace una petición xhr desde el navegador entonces cuando vamos a utilizar los pasos anteriores y hacemos click en la url dentro de "tipografias" como [esta](https://d26ik0aqrns2ec.cloudfront.net/Dox/Regular.woff2) nos tira un error `403 ERROR The request could not be satisfied.`

Entoces para descargarla vamos a abrir la consola en el navegador (F12):

1. Ir a la ventana "red" o "network"

2. buscamos el archivo .ttf o .woff de nuestra tipografía a descargar

![img-1]({{ site.url }}/img/licuadora/consola-web3.png)

![img-1]({{ site.url }}/img/licuadora/consola-web4.png)

3. Copiamos como cURL la petición que hace el navegador (gracias [gahen](https://github.com/Gahen/) por el tip) y agregamos al final: `--output nombredelatipografia.ttf`(poner la extención que estás descargando de tipografía)
```curl
curl 'https://d26ik0aqrns2ec.cloudfront.net/Dox/Regular.ttf' -H 'User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0' -H 'Accept: */*' -H 'Accept-Language: es-AR' --compressed -H 'Origin: https://www.fontfabric.com' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: https://www.fontfabric.com/fonts/dox/' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' -H 'TE: Trailers' --output dox-regular.ttf
```
> "El principal propósito y uso para cURL es automatizar transferencias de archivos o secuencias de operaciones no supervisadas. Es por ejemplo, una herramienta válida para simular las acciones de usuarios en un navegador web. [wiki](https://es.wikipedia.org/wiki/CURL)"


4. Pegamos la petición en una terminal
![img-1]({{ site.url }}/img/licuadora/consola-web5.png)

5. Enter y descargamos :D
![img-1]({{ site.url }}/img/licuadora/consola-web6.png)

## update 29-08-2021

Hay algunos casos donde las typos están en base64 hardcodeadas, por ejemplo en esta [web](https://frerejones.com/families/magnet)
si vamos a la pestaña de Tipografias podemos ver el link al "data:font/woff;charset=utf-8;base64"

![img-1]({{ site.url }}/img/licuadora/consola-web7.png)

vamos a generar una etiqueta href para poder descargar esa información pero ya decodificada:

en cualquier lado del html hacemos click derecho + editar como html y pegamos esto:
```html
<a download="nombre-typo.woff" href="data:font/woff;charset=utf-8;base64,asdasd...">Download</a>
```
gracias [Minko Gechev](https://stackoverflow.com/questions/14011021/how-to-download-a-base64-encoded-image/14011075#14011075)

reemplazar lo que esta entre "" en el href por el link largo que vimos en la imagen anterior de la tipografía a descargar.

luego hacemos click en cualquier lado de la web y aparecerá la palabra download en algun lado. pulsamos y descargamos nuestra nombre-typo.woff, luego convertimos como en pasos anteriores a .otf

![img-1]({{ site.url }}/img/licuadora/consola-web8.png)

_descargar_ para no perder acceso.
