---
layout: post
title: '/Consola web tu mejor amiga II [cinear]'
born: '2020'
date: '2020-08-20 23:59:00 -0300'
categories: licuadora
author: librenauta
---
### _Consola web for fun and piracy_

_este tutotial requiere de una consola gnu/linux, y el programa ffmpeg_

cine.ar es un sitio de producciones nacionales de argentina, donde se pueden ver películas de forma gratuita :3

Aquí una de las  formas de cómo descargarlas:

Es necesario loguearte para poder acceder a las películas. esto permite tener un token de validación al obtener los links que necesitaremos
![img-1]({{ site.url }}/img/licuadora/consola-2/consola-web-2-1.png)

Cuando apretás F12 para inspeccionar urls aparece este cartel.
![img-1]({{ site.url }}/img/licuadora/consola-2/consola-web-2-0.png)

XD

## ~Update 01-09-2021

Parece que se cambio el reproductor a [jwplayer] y la forma de servir el contenido, asique acá van unos ajustes y ordenar un poco la info nueva:

1. Como siempre hay que estar login.
2. ahora al apretar en RED (luego de F12 en el inspector de elementos) vamos a pulsar(para filtrar) sobre la etiqueta html y buscar "convert.php".

En "RED" es el lugar donde vemos todo lo que se descarga en nuestro navegador (DOM) si pulsamos después de entrar a la película, vamos a tener que recargar la página así hace las peticiones nuevamente y las podemos ver.

![img-1]({{ site.url }}/img/licuadora/consola-2/consola-web-2-7.png)

3. convert.php lo que hace es contener una listado de diferentes calidades de la película o documental.   hacemos click derecho y abrimos en otra página. allí aparecerá un texto muy largo(hash) pulsamos click derecho ver código fuente de la página y veremos algo así:

![img-1]({{ site.url }}/img/licuadora/consola-2/consola-web-2-8.png)

esa lista que vemos contiene la película en 240, 480, 720, específicamente la última es la de 720. pero no es una url como teníamos antes, si no que contiene una lista de las partes de la película hardcodeada en base64.

lo que vamos a hacer es decodificarlo de forma local para poder luego descargarlo como antes.

todas las versiones de gnu/linux o las más populares(? tiene base64 por defecto en la terminal asique utilizaremos eso para decodificarlo.

4. seleccionamos haciendo click después de: "data:application/x-mpegurl;base64," todo el "hash" (despues del "base64," es lo que nos importa)

5. abrimos un editor de texto (ej nano):
`$ nano movie-720.txt`

y apretamos ctrl+shift+c para pegar el hash. guardamos (puede tardar un ratito).

6. ahora que tenemos la lista de forma local lo vamos a decodificar, ese archivo que bajamos tiene la película partida en muchos pedacitos [Transmisión HLS :( ]) que luego vamos a unir con ffmpeg.

7. decodificar base64 la lista

`$ base64 --decode movie-720.txt > d-movie-720.txt`

8. como ahora le vamos a pasar un parametro "local" (d-movie-720.txt) a ffmpeg, tenemos que avisarle que no es una url y es un file. para esto antes del -i tenemos que sumar una flag: `-protocol_whitelist file,http,https,tcp,tls` [fuente](https://stackoverflow.com/questions/50455695/why-does-ffmpeg-ignore-protocol-whitelist-flag-when-converting-https-m3u8-stream)

`$ ffmpeg -protocol_whitelist file,http,https,tcp,tls -i d-movie-720.txt -bsf:a aac_adtstoasc -vcodec copy -c copy -crf 0 movie.mp4`

ffmpeg nos dejará un archivo movie.mp4 con la película completa en 720p

* _Gracias nulo por siempre cranear juntxs esto_
* _Gracias thaiel por el typo_  

9. y comienza la party-descarga!

![img-1]({{ site.url }}/img/licuadora/consola-2/consola-web-2-9.png)

_descargar_ para no perder acceso.

## histórico/anecdótico

![img-1]({{ site.url }}/img/licuadora/consola-2/consola-web-2-2.png)

Al querer llegar al link de la película para poder descargarla me encuentro con un archivo .m3u8 (playlist) que contiene un conjunto urls con archivos .ts
es el archivo de video cortado en cientos de archivos más pequeños.

![img-1]({{ site.url }}/img/licuadora/consola-2/consola-web-2-3.png)

Con ctrl + F es posible buscar directamente [.m3u8]()
![img-1]({{ site.url }}/img/licuadora/consola-2/consola-web-2-4.png)

![img-1]({{ site.url }}/img/licuadora/consola-2/consola-web-2-5.png)

y podemos copiar la url de esa lista para utilizarla para la descarga.

obtenemos algo parecido a esto: `https://564e2be0304b6.streamlock.net/odeon/_definst_/smil:omc/INCAA/5225/media.smil/chunklist_w2092967802_b260000_t64MjQwcCAoMTAwayk=.m3u8?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgwNDc2NjIuMDYxNTc2LCAic2lkIjogNTIyNSwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.B8TCHExiCe6eaCGVua_oULdgh4m06vBKomEX-ekLdYDHnZpbRYuc0TYsnu_nxzO5l6s4SH2NhLuc0ivP0kGbf38f8CSLc2CCUFa16jY50jHElxTcaO5BVqUdnO37ukzQY7cm2YfDnZi-jsujX72mSkSln--RDQKQXBerIOX0Ma86xwaDQiWNUBkKIvH6EI9mtmydDJK5ZnU2EotuahErtQQSIHzC8d---V9SMqWIZ4PWHVyAckYMShyddxHf1mk2P1I47HvwuY4sITILSfGqNxK-F5-t8rUv5l0ZaXmWoVVgRstp2boq8ZU0HpgKRiULqdJISLmYvu0t61vWXP0e_A`

lo que le sigue al .m3u8 es el token por estar logueades. sin esto no tendríamos acceso al recurso. `?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgwNDc2NjIuMDYxNTc2LCAic2lkIjogNTIyNSwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.B8TCHExiCe6eaCGVua_oULdgh4m06vBKomEX-ekLdYDHnZpbRYuc0TYsnu_nxzO5l6s4SH2NhLuc0ivP0kGbf38f8CSLc2CCUFa16jY50jHElxTcaO5BVqUdnO37ukzQY7cm2YfDnZi-jsujX72mSkSln--RDQKQXBerIOX0Ma86xwaDQiWNUBkKIvH6EI9mtmydDJK5ZnU2EotuahErtQQSIHzC8d---V9SMqWIZ4PWHVyAckYMShyddxHf1mk2P1I47HvwuY4sITILSfGqNxK-F5-t8rUv5l0ZaXmWoVVgRstp2boq8ZU0HpgKRiULqdJISLmYvu0t61vWXP0e_A`

# Ahora para descargar (linux shell)
* es necesario tener instalado [ffmpeg](https://ffmpeg.org/download.html)

en [este]( https://gist.github.com/tzmartin/fb1f4a8e95ef5fb79596bd4719671b5d) gist listan formas de descargar y convertir a un solo archivo una lista m3u8 :D,

lo seguimos! `ffmpeg -i "http://host/folder/file.m3u8" -bsf:a aac_adtstoasc -vcodec copy -c copy -crf 0 movie.mp4`

reemplazando la url "http://host/folder/file.m3u8" por la url que encontramos inspeccionando y comienza la descarga.

`[hls @ 0x56116ebe3d80] Skip ('#EXT-X-VERSION:3')
[hls @ 0x56116ebe3d80] Opening 'https://538d0bde28ccf.streamlock.net/odeon/_definst_/smil:omc/INCAA/6588/media.smil/media_w1229341901_b810000_t64MzYwcCAoNjUwayk=_0.ts?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgwNTIwOTYuNDQwMDI3LCAic2lkIjogNjU4OCwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.EojLKxwj-DqsHf1cuS-ZwqA9Tj-soDIr01d1_mDNoDRdT6NF_LJKN7HZ5puMmSSxeTQuEX1tqdeodjGQ4cyQQ_yL-mW6yy6LvWETlAfTchki4J5N3pWvb5AFeHHdqvgMeqZCQislhU32BvWdUPaZGzyomd2RCnasj1AvBFLoPnfmtS8GbyKKcJklLuxuZdWsTb_uxNxfPaPcr8lwTot2SmFe3AcTxuJ6-Ld7cXb2q444C6978cMl-GVUM1Q8Pu9dvqY3ZoA0AxQaS3OAafXVmMcUMX3MR5NSsMsrCEpanLwK2Z9dv-Qh_bJVGnC57X9jPgo94QevlxbzL8mz_Yplxw' for reading
[hls @ 0x56116ebe3d80] Opening 'https://538d0bde28ccf.streamlock.net/odeon/_definst_/smil:omc/INCAA/6588/media.smil/media_w1229341901_b810000_t64MzYwcCAoNjUwayk=_1.ts?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgwNTIwOTYuNDQwMDI3LCAic2lkIjogNjU4OCwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.EojLKxwj-DqsHf1cuS-ZwqA9Tj-soDIr01d1_mDNoDRdT6NF_LJKN7HZ5puMmSSxeTQuEX1tqdeodjGQ4cyQQ_yL-mW6yy6LvWETlAfTchki4J5N3pWvb5AFeHHdqvgMeqZCQislhU32BvWdUPaZGzyomd2RCnasj1AvBFLoPnfmtS8GbyKKcJklLuxuZdWsTb_uxNxfPaPcr8lwTot2SmFe3AcTxuJ6-Ld7cXb2q444C6978cMl-GVUM1Q8Pu9dvqY3ZoA0AxQaS3OAafXVmMcUMX3MR5NSsMsrCEpanLwK2Z9dv-Qh_bJVGnC57X9jPgo94QevlxbzL8mz_Yplxw' for reading
Input #0, hls, from 'https://538d0bde28ccf.streamlock.net/odeon/_definst_/smil:omc/INCAA/6588/media.smil/chunklist_w1229341901_b810000_t64MzYwcCAoNjUwayk=.m3u8?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgwNTIwOTYuNDQwMDI3LCAic2lkIjogNjU4OCwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.EojLKxwj-DqsHf1cuS-ZwqA9Tj-soDIr01d1_mDNoDRdT6NF_LJKN7HZ5puMmSSxeTQuEX1tqdeodjGQ4cyQQ_yL-mW6yy6LvWETlAfTchki4J5N3pWvb5AFeHHdqvgMeqZCQislhU32BvWdUPaZGzyomd2RCnasj1AvBFLoPnfmtS8GbyKKcJklLuxuZdWsTb_uxNxfPaPcr8lwTot2SmFe3AcTxuJ6-Ld7cXb2q444C6978cMl-GVUM1Q8Pu9dvqY3ZoA0AxQaS3OAafXVmMcUMX3MR5NSsMsrCEpanLwK2Z9dv-Qh_bJVGnC57X9jPgo94QevlxbzL8mz_Yplxw':
  Duration: 01:02:13.85, start: 0.000000, bitrate: 0 kb/s
  Program 0
    Metadata:
      variant_bitrate : 0
    Stream #0:0: Data: timed_id3 (ID3  / 0x20334449)
    Metadata:
      variant_bitrate : 0
    Stream #0:1: Video: h264 (Constrained Baseline) ([27][0][0][0] / 0x001B), yuv420p(tv, bt709), 530x360, 25 fps, 25 tbr, 90k tbn, 50 tbc
    Metadata:
      variant_bitrate : 0
    Stream #0:2: Audio: aac (LC) ([15][0][0][0] / 0x000F), 48000 Hz, mono, fltp
    Metadata:
      variant_bitrate : 0
Stream mapping:
  Stream #0:1 -> #0:0 (copy)
  Stream #0:2 -> #0:1 (aac (native) -> aac (native))
Press [q] to stop, [?] for help
Output #0, mp4, to 'file.mp4':
  Metadata:
    encoder         : Lavf58.29.100
    Stream #0:0: Video: h264 (Constrained Baseline) (avc1 / 0x31637661), yuv420p(tv, bt709), 530x360, q=2-31, 25 fps, 25 tbr, 90k tbn, 90k tbc
    Metadata:
      variant_bitrate : 0
    Stream #0:1: Audio: aac (LC) (mp4a / 0x6134706D), 48000 Hz, mono, fltp, 69 kb/s
    Metadata:
      variant_bitrate : 0
      encoder         : Lavc58.54.100 aac
[https @ 0x56116ecff940] Opening 'https://538d0bde28ccf.streamlock.net/odeon/_definst_/smil:omc/INCAA/6588/media.smil/media_w1229341901_b810000_t64MzYwcCAoNjUwayk=_2.ts?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgwNTIwOTYuNDQwMDI3LCAic2lkIjogNjU4OCwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.EojLKxwj-DqsHf1cuS-ZwqA9Tj-soDIr01d1_mDNoDRdT6NF_LJKN7HZ5puMmSSxeTQuEX1tqdeodjGQ4cyQQ_yL-mW6yy6LvWETlAfTchki4J5N3pWvb5AFeHHdqvgMeqZCQislhU32BvWdUPaZGzyomd2RCnasj1AvBFLoPnfmtS8GbyKKcJklLuxuZdWsTb_uxNxfPaPcr8lwTot2SmFe3AcTxuJ6-Ld7cXb2q444C6978cMl-GVUM1Q8Pu9dvqY3ZoA0AxQaS3OAafXVmMcUMX3MR5NSsMsrCEpanLwK2Z9dv-Qh_bJVGnC57X9jPgo94QevlxbzL8mz_Yplxw' for reading
`

(me pasó de conectar y acceder a una lista de m3u8 con el contenido en menor calidad (se ve que mi conexión inicial en ese intento fue de velocidad baja) pero al refrescar, me entregó otra playlist con videos de muchisima mejor calidad, de algúna manera que todavía no sé, debe ser posible hacer la petición con la mejor calidad siempre sin depender de la conexión)

## ~Update 24-08-2020

Es posible encontrar la petición que tiene las urls de _todas_ las calidades disponibles bajo una [XHR](https://es.wikipedia.org/wiki/XMLHttpRequest), solo en la consola (estando en la pestaña RED) hay que pulsar XHR para filtrar. y ubicar la que diga convert.php que da como resultado un archivo .html de texto plano similar al siquiente.

![img-1]({{ site.url }}/img/licuadora/consola-2/consola-web-2-6.png)

```JSON
#EXTM3U #EXT-X-VERSION:3 #EXT-X-STREAM-INF:BANDWIDTH=260000,NAME="240p (100k)",RESOLUTION=428x240,CLOSED-CAPTIONS=NONE https://564c854884942.streamlock.net/odeon/_definst_/smil:omc/INCAA/5466/media.smil/chunklist_w602460480_b260000_t64MjQwcCAoMTAwayk=.m3u8?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgzMDQxNTIuNjA0NTk4LCAic2lkIjogNTQ2NiwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.tpmCSdH0GO_rXcYTvZAPn41LJI3QIcTGu7X7au_33BqWUDVe00aadklXJySf6CcDpR4oP_FEpaWk-3dpTAv_xHzQ5qDWIqOCLsqcfbVWLBNTsUpIubVpuh5WnU96GlR7xZh_AgJk2pXGa5liksWeRqVLb39PNDyur8pADXxaILFSk0IMT7_a1DFpbxG7NeyvXjD4C-6Ev6xYCiVPvut9Stb7fCwf5yuF8moGiA7H2s4QZSW6AjTgMz1nhOd9lYvdKzPfAEu3tulhZgXdwelQWvw7yRrjqnAWuUU51WrR_1XwDFvWZHIPI9EovK49maVlyIjkmLYYVNao6FO4j1hjfw #EXT-X-STREAM-INF:BANDWIDTH=360000,NAME="240p (200k)",RESOLUTION=428x240,CLOSED-CAPTIONS=NONE https://564c854884942.streamlock.net/odeon/_definst_/smil:omc/INCAA/5466/media.smil/chunklist_w602460480_b360000_t64MjQwcCAoMjAwayk=.m3u8?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgzMDQxNTIuNjA0NTk4LCAic2lkIjogNTQ2NiwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.tpmCSdH0GO_rXcYTvZAPn41LJI3QIcTGu7X7au_33BqWUDVe00aadklXJySf6CcDpR4oP_FEpaWk-3dpTAv_xHzQ5qDWIqOCLsqcfbVWLBNTsUpIubVpuh5WnU96GlR7xZh_AgJk2pXGa5liksWeRqVLb39PNDyur8pADXxaILFSk0IMT7_a1DFpbxG7NeyvXjD4C-6Ev6xYCiVPvut9Stb7fCwf5yuF8moGiA7H2s4QZSW6AjTgMz1nhOd9lYvdKzPfAEu3tulhZgXdwelQWvw7yRrjqnAWuUU51WrR_1XwDFvWZHIPI9EovK49maVlyIjkmLYYVNao6FO4j1hjfw #EXT-X-STREAM-INF:BANDWIDTH=510000,NAME="270p (350k)",RESOLUTION=480x270,CLOSED-CAPTIONS=NONE https://564c854884942.streamlock.net/odeon/_definst_/smil:omc/INCAA/5466/media.smil/chunklist_w602460480_b510000_t64MjcwcCAoMzUwayk=.m3u8?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgzMDQxNTIuNjA0NTk4LCAic2lkIjogNTQ2NiwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.tpmCSdH0GO_rXcYTvZAPn41LJI3QIcTGu7X7au_33BqWUDVe00aadklXJySf6CcDpR4oP_FEpaWk-3dpTAv_xHzQ5qDWIqOCLsqcfbVWLBNTsUpIubVpuh5WnU96GlR7xZh_AgJk2pXGa5liksWeRqVLb39PNDyur8pADXxaILFSk0IMT7_a1DFpbxG7NeyvXjD4C-6Ev6xYCiVPvut9Stb7fCwf5yuF8moGiA7H2s4QZSW6AjTgMz1nhOd9lYvdKzPfAEu3tulhZgXdwelQWvw7yRrjqnAWuUU51WrR_1XwDFvWZHIPI9EovK49maVlyIjkmLYYVNao6FO4j1hjfw #EXT-X-STREAM-INF:BANDWIDTH=810000,NAME="360p (650k)",RESOLUTION=640x360,CLOSED-CAPTIONS=NONE https://564c854884942.streamlock.net/odeon/_definst_/smil:omc/INCAA/5466/media.smil/chunklist_w602460480_b810000_t64MzYwcCAoNjUwayk=.m3u8?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgzMDQxNTIuNjA0NTk4LCAic2lkIjogNTQ2NiwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.tpmCSdH0GO_rXcYTvZAPn41LJI3QIcTGu7X7au_33BqWUDVe00aadklXJySf6CcDpR4oP_FEpaWk-3dpTAv_xHzQ5qDWIqOCLsqcfbVWLBNTsUpIubVpuh5WnU96GlR7xZh_AgJk2pXGa5liksWeRqVLb39PNDyur8pADXxaILFSk0IMT7_a1DFpbxG7NeyvXjD4C-6Ev6xYCiVPvut9Stb7fCwf5yuF8moGiA7H2s4QZSW6AjTgMz1nhOd9lYvdKzPfAEu3tulhZgXdwelQWvw7yRrjqnAWuUU51WrR_1XwDFvWZHIPI9EovK49maVlyIjkmLYYVNao6FO4j1hjfw #EXT-X-STREAM-INF:BANDWIDTH=1242000,NAME="480p (1050k)",RESOLUTION=854x480,CLOSED-CAPTIONS=NONE https://564c854884942.streamlock.net/odeon/_definst_/smil:omc/INCAA/5466/media.smil/chunklist_w602460480_b1242000_t64NDgwcCAoMTA1MGsp.m3u8?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgzMDQxNTIuNjA0NTk4LCAic2lkIjogNTQ2NiwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.tpmCSdH0GO_rXcYTvZAPn41LJI3QIcTGu7X7au_33BqWUDVe00aadklXJySf6CcDpR4oP_FEpaWk-3dpTAv_xHzQ5qDWIqOCLsqcfbVWLBNTsUpIubVpuh5WnU96GlR7xZh_AgJk2pXGa5liksWeRqVLb39PNDyur8pADXxaILFSk0IMT7_a1DFpbxG7NeyvXjD4C-6Ev6xYCiVPvut9Stb7fCwf5yuF8moGiA7H2s4QZSW6AjTgMz1nhOd9lYvdKzPfAEu3tulhZgXdwelQWvw7yRrjqnAWuUU51WrR_1XwDFvWZHIPI9EovK49maVlyIjkmLYYVNao6FO4j1hjfw #EXT-X-STREAM-INF:BANDWIDTH=1792000,NAME="600p (1600k)",RESOLUTION=1068x600,CLOSED-CAPTIONS=NONE https://564c854884942.streamlock.net/odeon/_definst_/smil:omc/INCAA/5466/media.smil/chunklist_w602460480_b1792000_t64NjAwcCAoMTYwMGsp.m3u8?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgzMDQxNTIuNjA0NTk4LCAic2lkIjogNTQ2NiwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.tpmCSdH0GO_rXcYTvZAPn41LJI3QIcTGu7X7au_33BqWUDVe00aadklXJySf6CcDpR4oP_FEpaWk-3dpTAv_xHzQ5qDWIqOCLsqcfbVWLBNTsUpIubVpuh5WnU96GlR7xZh_AgJk2pXGa5liksWeRqVLb39PNDyur8pADXxaILFSk0IMT7_a1DFpbxG7NeyvXjD4C-6Ev6xYCiVPvut9Stb7fCwf5yuF8moGiA7H2s4QZSW6AjTgMz1nhOd9lYvdKzPfAEu3tulhZgXdwelQWvw7yRrjqnAWuUU51WrR_1XwDFvWZHIPI9EovK49maVlyIjkmLYYVNao6FO4j1hjfw #EXT-X-STREAM-INF:BANDWIDTH=2542000,NAME="720p (2350k)",RESOLUTION=1280x720,CLOSED-CAPTIONS=NONE https://564c854884942.streamlock.net/odeon/_definst_/smil:omc/INCAA/5466/media.smil/chunklist_w602460480_b2542000_t64NzIwcCAoMjM1MGsp.m3u8?t=eyJhbGciOiJSUzI1NiJ9.eyJzb3VyY2UiOiAiSU5DQUEiLCAiY3RyeSI6ICJBUiIsICJ1aWQiOiAiNWI4YjJiZGE1ZTk3MDU2NmJiNzA1OGY1IiwgImV4cCI6IDE1OTgzMDQxNTIuNjA0NTk4LCAic2lkIjogNTQ2NiwgInBpZFBhcmFtTmFtZSI6ICI1YjhiMmMxN2U5ZDQzZTEyZjk2ZTc2MjMifQ.tpmCSdH0GO_rXcYTvZAPn41LJI3QIcTGu7X7au_33BqWUDVe00aadklXJySf6CcDpR4oP_FEpaWk-3dpTAv_xHzQ5qDWIqOCLsqcfbVWLBNTsUpIubVpuh5WnU96GlR7xZh_AgJk2pXGa5liksWeRqVLb39PNDyur8pADXxaILFSk0IMT7_a1DFpbxG7NeyvXjD4C-6Ev6xYCiVPvut9Stb7fCwf5yuF8moGiA7H2s4QZSW6AjTgMz1nhOd9lYvdKzPfAEu3tulhZgXdwelQWvw7yRrjqnAWuUU51WrR_1XwDFvWZHIPI9EovK49maVlyIjkmLYYVNao6FO4j1hjfw
```
y ponemos a descargar la que diga 720p (2350k) :D
