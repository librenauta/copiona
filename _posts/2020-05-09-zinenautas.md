---
layout: post
title: '#zinenautas'
born: '2017'
date: '2020-05-09 13:25:00 -0300'
categories: proyectos
author: librenauta
---
## Un web/paper zine

## zinenauta

>Zinenauta, web | físico zine de autodefensa digital y cultura libre && internet's

Zinenauta es una exploración visual en proceso constante, aquí podrás ver nuestra relación con las tecnologías junto con una experiencia de cómo bajar a papel, una parte de internet. Dentro de este fanzine encontrarás contenido de +culturalibre +code +telegráfica +privacidad +anonimato +collagedigital. {en papel es hermoso}

Zi-ne-nau-ta será una publicación de 10 ejemplares, la primera serie de #5 invitan a reflexionar sobre la distopía de las telecomunicaciones y cómo vivímos dentro de internet en el contexto de una sociedad de control, por otra parte las siguientes 5 explorarán todas las aristas que creemos valiosas en la red desde que la conocemos, soy fan de internet. no puedo solo criticarlo hay cosas increíbles. escribir y publicar esto con herramientas creadas por comunidades libres es increíble.

ver online en: --> [zinenauta.copiona.com](https://zinenauta.copiona.com)

![zinenautas]({{ url.site }}/img/zinenauta-0.jpg)

## Los primeros #5

![img]({{ site.url }}/img/z1.png)

![img]({{ site.url }}/img/z2.png)

![img]({{ site.url }}/img/z3.png)

![img]({{ site.url }}/img/z4.png)

![img]({{ site.url }}/img/z5.png)

podés comprarlos en papel en el [shop](https://shop.copiona.com/zinenauta.html) <3
