---
layout: post
title: '#Collagetober'
born: '2019'
date: '2019-10-08 12:20:00 -0300'
categories: diseño
author: librenauta
---
# Collagetober  | 2019

#1 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/1.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/1.png)

#2 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/2.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/2.png)

#3 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/3.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/3.png)

#4 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/4.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/4.png)

#5 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/5.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/5.png)

#6 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/6.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/6.png)

#7 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/7.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/7.png)

#8 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/8.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/8.png)

#9 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/9.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/9.png)

#10 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/10.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/10.png)

#11 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/11.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/11.png)

#12 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/12.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/12.png)

#13 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/13.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/13.png)

#14 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/14.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/14.png)

#15 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/15.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/15.png)

#16 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/16.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/16.png)

#17 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/17.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/17.png)

#18 - Para bajar el .svg editable pulsar [[aquí]](https://shop.copiona.com/images/collagetober-2019/18.svg)

![collage, inktober]({{ site.url }}/img/collagetober-2019/18.png)
