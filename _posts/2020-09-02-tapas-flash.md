---
layout: post
title: '#Tapas Flash'
born: '2020'
date: '2020-09-02 10:10:00 -0300'
categories: diseño
author: librenauta
---
# Tapas Flash para editorial flash #

 > Tapas flash es un mini proyecto de editorial flash para diseñar tapas y asignar el tiempo que me llevó hacerlas en la contratapa, buscando partes de otras imágenes, y haciendo colash vectorial <3


#### Visual Poetry #3
Me imprimi unas tapas flash que me diseñe para un nuevo cuaderno: filmina | adhesivo flujo | contac chroma | opalina 120gr #exp #rmx

![visual]({{ url.site }}/img/dis/visual-poetry-cover.png)

![visual]({{ url.site }}/img/dis/visual-poetry-cover-1.jpg)
![visual]({{ url.site }}/img/dis/visual-poetry-cover-2.jpg)
![visual]({{ url.site }}/img/dis/visual-poetry-cover-3.jpg)
![visual]({{ url.site }}/img/dis/visual-poetry-cover-4.jpg)

1. [link a pdf]({{ url.site }}/img/dis/visual-poetry-cover.pdf)

#### Chaos poetry <3 #2

![visual]({{ url.site }}/img/dis/chaos-poetry-2021.png)

Pueden descargar e imprimirse sus propias tapas <3 (están en formato A4)

1. [link a svg]({{ url.site }}/img/dis/chaos-poetry-2021.svg)
2. [link a pdf]({{ url.site }}/img/dis/chaos-poetry-2021.pdf)

Esta tapa fue diseñada para el calendario del [2021] que me compartió [jine](https://www.nervousdata.com/) via [mastodon](https://sonomu.club/@jine/105697515019410041)
[link en otros idiomas](https://nervousdata.com/kalender.html)
[* PDF páginas individuales ES](https://www.nervousdata.com/files/1semana1pagina_2021_es.pdf)
[* PDF para imprimir (imposición) ES](https://www.nervousdata.com/files/1semana1pagina_2021_es_print.pdf)

#### Visual Poetry <3 #1
![visual]({{ url.site }}/img/dis/visual-poetry.jpg)

![visual]({{ url.site }}/img/dis/visual-poetry-2.jpg)

Pueden descargar e imprimirse sus propias tapas <3 (están en A4)

1. [link a svg]({{ url.site }}/img/dis/visual-poetry.svg)
2. [link a pdf]({{ url.site }}/img/dis/visual-poetry.pdf)

_un glitch scan_
![un glitch]({{ url.site }}/img/proyectos/editorial-flash-glitch.jpg)
